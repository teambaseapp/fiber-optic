.. optical-fibre documentation master file, created by
   sphinx-quickstart on Mon Nov 16 10:48:27 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to optical-fibre's documentation!
=========================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Building Linux
--------------

        a. Download from http://buildroot.uclibc.org/downloads/buildroot-2015.05.tar.gz. Use attached buldroot config, as .config, basic config is 
	   ::
           	Target Architecture (ARM (little endian))  --->
           	Target Binary Format (ELF)  --->
           	Target Architecture Variant (cortex-A9)  --->
           	Target ABI (EABIhf)  --->
           	Enable NEON SIMD extension support
           	Floating point strategy (VFPv3)  --->
           	ARM instruction set (Thumb2)  --->

        b. Do 'make freescale_imx6sololiteevk_defconfig' to set configuration for imx6. Then run command 'make' to build the buildroot.

        c. Root filesystem just created is located in 'output/images/' as rootfs.tar .

        d. install arm-linux-gnueabihf- toolchain from sudo apt-get install 
	   gcc-arm-linux-gnueabihf ::
           	git clone -b tegra git://git.toradex.com/linux-toradex.git
           	export ARCH=arm
           	export CROSS_COMPILE=arm-linux-gnueabihf-

        e. Download kernel source using git as git clone -b <latest-imx6-toradex-branch>
	   git://git.toradex.com/linux-toradex.git then do ::
           	cd linux-toradex
           	make colibri-imx6-defconfig

        f. To compile the kernel do 
	   ::
           	make -j3 uImage LOADADDR=10008000 2>&1 | tee build.log
           	make  imx6dl-colibri-eval-v3.dtb

        g. At this point one may also compile the kernel modules 
	   ::
           	make -j3 modules

        h. Download http://developer1.toradex.com/files/toradex-dev/uploads/media/Colibri/Linux/Images/Colibri_iMX6_LinuxImageV2.5Beta2_20151106.tar.bz2,
           extract into Colibri_iMX6_LinuxImageV2.5 directory and do ::
           	cd Colibri_iMX6_LinuxImageV2.5 
           	mv rootfs/ angstrom-rootfs/
           	mkdir rootfs
           	cd rootfs
           	sudo tar xvf /path/to/buildroot-2015.5/output/images/rootfs.tar (sudo required)

        i. Colibri BSP needs rootfs/etc/issue to contain single line colibri-imx6.

        j. Copy uImage contained in /path_to_linux-toradex/arch/arm/boot/ to /path_to_Colibri_iMX6_LinuxImageV2.5/colibri-imx6_bin/ and 
	   then do ::
           	cd /path/to/toradex-linux/
           	cp arch/arm/boot/uImage /path/to/Colibri_iMX6_LinuxImageV2.5/colibri-imx6_bin/
           	sudo make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- modules_install  INSTALL_MOD_PATH=/path/to/Colibri_iMX6_LinuxImageV2.5/rootfs/


Building uboot
--------------

Flashing the Board
------------------

        a. Format a microsd card as fat32/vfat. Attach to computer, mount point is 
	   example /media/ABCD-1234/ ::
           	cd Colibri_T30_LinuxImageV2/
           	./update.sh -o /media/ABCD-1234/

        b. Eject microsd after completion, insert into Iris devkit board slot. Attach usb-uart on Iris serial port, open minicom or other serial monitor. Power on iris board,
           immediately press keys to stop uboot. Run: run setupdate & run update ::
           	run setupdate
           	run update



