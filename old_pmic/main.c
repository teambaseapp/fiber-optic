//main.c
#include "stm8s.h"
#include "stm8s_conf.h"
#include <stm8s_gpio.h>
#include "board.h"

void gpio_init(void)
{
  GPIO_DeInit(GPIOC);
  GPIO_DeInit(GPIOD);
  GPIO_Init(STM_LED_PORT, STM_LED_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
  GPIO_Init(PWR_LED_PORT, PWR_LED_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
  GPIO_Init(PWR_BTN_PORT, PWR_BTN_PIN, GPIO_MODE_IN_FL_NO_IT);
  GPIO_Init(PWR_CTRL_PORT, PWR_CTRL_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
}


void __delay__(int t)
{
   unsigned int i,j;
   for(j=0;j<t;j++)
    for(i=0;i<64000;i++);
}



int main( void )
{
    int i=0, status = OFF;
    unsigned long int count_off = 64000;
    gpio_init();

    GPIO_WriteLow(STM_LED_PORT, STM_LED_PIN);
     __delay__(2);
    GPIO_WriteHigh(PWR_LED_PORT, PWR_LED_PIN);

    
while(1)
  {
         if( GPIO_ReadInputPin(PWR_BTN_PORT, PWR_BTN_PIN) )
         {
                     if ( status == OFF ) {
                                    __delay__(2);
                                    GPIO_WriteLow(PWR_LED_PORT, PWR_LED_PIN);
                                    GPIO_WriteHigh(PWR_CTRL_PORT, PWR_CTRL_PIN);
                                    status = ON;
                                          }
           


                     else {
                              while(count_off-- != 0)
                              {
                                  if( GPIO_ReadInputPin(PWR_BTN_PORT, PWR_BTN_PIN) )
                                  {
                                     if( count_off == 0)
                                    {
                                          uart_print_string("HALT");                            //uart halt message is to be sent
                                          __delay__(2);
                                          GPIO_WriteLow(PWR_CTRL_PORT, PWR_CTRL_PIN);
                                          GPIO_WriteHigh(PWR_LED_PORT, PWR_LED_PIN);
                                          status = OFF;
                                          __delay__(10);
                                    }
                                  }
                                  else
                                  {
                                    break;
                                  }
                              }
                              count_off = 64000;
                          }
         }
         
   }

return 0;

}
