//uart.c

#include "../inc/uart.h"

void uart_config(void)
{
  UART1_DeInit();
  UART1_Init( 15200, UART1_WORDLENGTH_8D, UART1_STOPBITS_1, UART1_PARITY_NO, UART1_SYNCMODE_CLOCK_DISABLE, UART1_MODE_TXRX_ENABLE); 
  CLK_PeripheralClockConfig( CLK_PERIPHERAL_UART1, ENABLE);
  UART1_Cmd(ENABLE);
}
  
void uart_print_string( uint8_t *str )
{
      while( !(*str) )
      {
        while(UART1_GetFlagStatus(UART1_FLAG_TXE) != SET);
        UART1_SendData8( *str++ );
      }
}
