//board.h

#include "libraries/inc/uart.h"

#define STM_LED_PORT    GPIOC
#define STM_LED_PIN     GPIO_PIN_7

#define PWR_LED_PORT    GPIOD
#define PWR_LED_PIN     GPIO_PIN_3

#define PWR_BTN_PORT    GPIOD
#define PWR_BTN_PIN     GPIO_PIN_2

#define PWR_CTRL_PORT   GPIOA
#define PWR_CTRL_PIN    GPIO_PIN_3

#define OFF             0
#define ON              1
#define SET             1
#define RESET           0