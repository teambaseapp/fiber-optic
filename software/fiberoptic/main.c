//main.c
#include "stm8s.h"
#include "stm8s_conf.h"
#include <stm8s_gpio.h>
#include "board.h"

void gpio_init(void)
{
  GPIO_DeInit(GPIOC);
  GPIO_DeInit(GPIOD);
  GPIO_Init(STM_LED_PORT, STM_LED_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
  GPIO_Init(PWR_LED_PORT, PWR_LED_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
  GPIO_Init(PWR_BTN_PORT, PWR_BTN_PIN, GPIO_MODE_IN_FL_NO_IT);
  GPIO_Init(PWR_CTRL_PORT, PWR_CTRL_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
}


void __delay__(int t)
{
   unsigned int i,j;
   for(j=0;j<t;j++)
    for(i=0;i<64000;i++);
}




int main( void )
{
    int  status = OFF , shutdown = RESET;
    //char resp;
    unsigned long int count_off = 64000;
    gpio_init();
    uart_config();
    GPIO_WriteLow(STM_LED_PORT, STM_LED_PIN);
     __delay__(2);
    GPIO_WriteHigh(PWR_LED_PORT, PWR_LED_PIN);


    
while(1)
  {
         if( GPIO_ReadInputPin(PWR_BTN_PORT, PWR_BTN_PIN) )
         {
                     if ( status == OFF ) {
                                    __delay__(2);
                                    GPIO_WriteLow(PWR_LED_PORT, PWR_LED_PIN);
                                    GPIO_Init(PWR_CTRL_PORT, PWR_CTRL_PIN, GPIO_MODE_OUT_PP_LOW_FAST);
                                    GPIO_WriteHigh(PWR_CTRL_PORT, PWR_CTRL_PIN);
                                    status = ON;
                                    //while(GPIO_ReadInputPin(PWR_BTN_PORT, PWR_BTN_PIN) == RESET){};
                                    __delay__(100);
                                          }
                     
                   


                     else {
                                  while( count_off-- != 54000)
                                  {
                                    if( GPIO_ReadInputPin(PWR_BTN_PORT, PWR_BTN_PIN) )
                                    {
                                      if(count_off == 54000)
                                      {
                                      uart_print_string("halt");
                                      shutdown = SET;
                                      }
                                    }
                                  }
                                  count_off = 64000;
                                  
                                  
                              while(count_off-- != 0)
                              {
                                  if( GPIO_ReadInputPin(PWR_BTN_PORT, PWR_BTN_PIN) )
                                  {
                                     if( count_off == 0)
                                    {
                                          //uart_print_string("halt");                            //uart halt message is to be sent
                                          //__delay__(2);
                                          GPIO_WriteHigh(PWR_LED_PORT, PWR_LED_PIN);
                                          GPIO_WriteLow(PWR_CTRL_PORT, PWR_CTRL_PIN);
                                          status = OFF;
                                          __delay__(15);
                                    }
                                  }
                                  else
                                  {
                                    break;
                                  }
                              }
                              count_off = 64000;
                          }
         } 
         
         if (UART1_GetFlagStatus(UART1_FLAG_RXNE) == SET)
         {
           if (UART1_ReceiveData8() == 's' && shutdown == SET){
            uart_print_string("system is shutting down !!");
            __delay__(50);
            status = OFF;
            GPIO_WriteHigh(PWR_LED_PORT, PWR_LED_PIN); 
            GPIO_WriteLow(PWR_CTRL_PORT, PWR_CTRL_PIN);
            shutdown = RESET;
            __delay__(5);
           }
         }
    }
         
   

//return 0;

}
