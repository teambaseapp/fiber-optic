<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="120" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="connectors">
<packages>
<package name="FFC-40PIN-0.5MM">
<smd name="40" x="-9.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="39" x="-9.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="38" x="-8.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="37" x="-8.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="36" x="-7.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="35" x="-7.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="34" x="-6.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="33" x="-6.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="32" x="-5.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="31" x="-5.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="30" x="-4.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="29" x="-4.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="28" x="-3.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="27" x="-3.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="26" x="-2.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="25" x="-2.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="24" x="-1.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="23" x="-1.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="22" x="-0.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="21" x="-0.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="20" x="0.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="19" x="0.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="18" x="1.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="1.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="2.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="2.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="3.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="3.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="4.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="4.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="5.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="5.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="6.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="6.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="7.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="7.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="8.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="8.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="9.25" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="1" x="9.75" y="0" dx="1.27" dy="0.3" layer="1" rot="R90"/>
<smd name="P$41" x="-11.5" y="-2.15" dx="3.5" dy="2.3" layer="1" rot="R90"/>
<smd name="P$42" x="11.5" y="-2.15" dx="3.5" dy="2.3" layer="1" rot="R90"/>
<wire x1="-10" y1="0" x2="-13" y2="0" width="0.127" layer="21"/>
<wire x1="-13" y1="0" x2="-13" y2="-4" width="0.127" layer="21"/>
<wire x1="-13" y1="-4" x2="-13.25" y2="-4.25" width="0.127" layer="21" curve="-90"/>
<wire x1="-13.25" y1="-4.25" x2="-13.5" y2="-4.25" width="0.127" layer="21"/>
<wire x1="-13.5" y1="-4.25" x2="-14" y2="-4.75" width="0.127" layer="21" curve="90"/>
<wire x1="-14" y1="-4.75" x2="-14" y2="-5.75" width="0.127" layer="21"/>
<wire x1="-14" y1="-5.75" x2="-13.5" y2="-6.25" width="0.127" layer="21" curve="90"/>
<wire x1="-13.5" y1="-6.25" x2="13.5" y2="-6.25" width="0.127" layer="21"/>
<wire x1="10" y1="0" x2="13" y2="0" width="0.127" layer="21"/>
<wire x1="13" y1="0" x2="13" y2="-4" width="0.127" layer="21"/>
<wire x1="13" y1="-4" x2="13.25" y2="-4.25" width="0.127" layer="21" curve="90"/>
<wire x1="13.25" y1="-4.25" x2="13.5" y2="-4.25" width="0.127" layer="21"/>
<wire x1="13.5" y1="-4.25" x2="14" y2="-4.75" width="0.127" layer="21" curve="-90"/>
<wire x1="14" y1="-4.75" x2="14" y2="-5.75" width="0.127" layer="21"/>
<wire x1="14" y1="-5.75" x2="13.5" y2="-6.25" width="0.127" layer="21" curve="-90"/>
<wire x1="-14" y1="-5.25" x2="14" y2="-5.25" width="0.127" layer="51"/>
<text x="-3.75" y="-2.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.75" y="-4.25" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="FFC-40PIN">
<pin name="P$1" x="-22.86" y="48.26" length="middle"/>
<pin name="P$2" x="-22.86" y="43.18" length="middle"/>
<pin name="P$3" x="-22.86" y="38.1" length="middle"/>
<pin name="P$4" x="-22.86" y="33.02" length="middle"/>
<pin name="P$5" x="-22.86" y="27.94" length="middle"/>
<pin name="P$6" x="-22.86" y="22.86" length="middle"/>
<pin name="P$7" x="-22.86" y="17.78" length="middle"/>
<pin name="P$8" x="-22.86" y="12.7" length="middle"/>
<pin name="P$9" x="-22.86" y="7.62" length="middle"/>
<pin name="P$10" x="-22.86" y="2.54" length="middle"/>
<pin name="P$11" x="-22.86" y="-2.54" length="middle"/>
<pin name="P$12" x="-22.86" y="-7.62" length="middle"/>
<pin name="P$13" x="-22.86" y="-12.7" length="middle"/>
<pin name="P$14" x="-22.86" y="-17.78" length="middle"/>
<pin name="P$15" x="-22.86" y="-22.86" length="middle"/>
<pin name="P$16" x="-22.86" y="-27.94" length="middle"/>
<pin name="P$17" x="-22.86" y="-33.02" length="middle"/>
<pin name="P$18" x="-22.86" y="-38.1" length="middle"/>
<pin name="P$19" x="-22.86" y="-43.18" length="middle"/>
<pin name="P$20" x="-22.86" y="-48.26" length="middle"/>
<pin name="P$21" x="22.86" y="-48.26" length="middle" rot="R180"/>
<pin name="P$22" x="22.86" y="-43.18" length="middle" rot="R180"/>
<pin name="P$23" x="22.86" y="-38.1" length="middle" rot="R180"/>
<pin name="P$24" x="22.86" y="-33.02" length="middle" rot="R180"/>
<pin name="P$25" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="P$26" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="P$27" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="P$28" x="22.86" y="-12.7" length="middle" rot="R180"/>
<pin name="P$29" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="P$30" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="P$31" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="P$32" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="P$33" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="P$34" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="P$35" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="P$36" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="P$37" x="22.86" y="33.02" length="middle" rot="R180"/>
<pin name="P$38" x="22.86" y="38.1" length="middle" rot="R180"/>
<pin name="P$39" x="22.86" y="43.18" length="middle" rot="R180"/>
<pin name="P$40" x="22.86" y="48.26" length="middle" rot="R180"/>
<wire x1="-17.78" y1="50.8" x2="-17.78" y2="-50.8" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-50.8" x2="17.78" y2="-50.8" width="0.254" layer="94"/>
<wire x1="17.78" y1="-50.8" x2="17.78" y2="50.8" width="0.254" layer="94"/>
<wire x1="17.78" y1="50.8" x2="-17.78" y2="50.8" width="0.254" layer="94"/>
<text x="-5.08" y="25.4" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FFC-40PIN" prefix="FFC">
<gates>
<gate name="G$1" symbol="FFC-40PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FFC-40PIN-0.5MM">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$10" pad="10"/>
<connect gate="G$1" pin="P$11" pad="11"/>
<connect gate="G$1" pin="P$12" pad="12"/>
<connect gate="G$1" pin="P$13" pad="13"/>
<connect gate="G$1" pin="P$14" pad="14"/>
<connect gate="G$1" pin="P$15" pad="15"/>
<connect gate="G$1" pin="P$16" pad="16"/>
<connect gate="G$1" pin="P$17" pad="17"/>
<connect gate="G$1" pin="P$18" pad="18"/>
<connect gate="G$1" pin="P$19" pad="19"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$20" pad="20"/>
<connect gate="G$1" pin="P$21" pad="21"/>
<connect gate="G$1" pin="P$22" pad="22"/>
<connect gate="G$1" pin="P$23" pad="23"/>
<connect gate="G$1" pin="P$24" pad="24"/>
<connect gate="G$1" pin="P$25" pad="25"/>
<connect gate="G$1" pin="P$26" pad="26"/>
<connect gate="G$1" pin="P$27" pad="27"/>
<connect gate="G$1" pin="P$28" pad="28"/>
<connect gate="G$1" pin="P$29" pad="29"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$30" pad="30"/>
<connect gate="G$1" pin="P$31" pad="31"/>
<connect gate="G$1" pin="P$32" pad="32"/>
<connect gate="G$1" pin="P$33" pad="33"/>
<connect gate="G$1" pin="P$34" pad="34"/>
<connect gate="G$1" pin="P$35" pad="35"/>
<connect gate="G$1" pin="P$36" pad="36"/>
<connect gate="G$1" pin="P$37" pad="37"/>
<connect gate="G$1" pin="P$38" pad="38"/>
<connect gate="G$1" pin="P$39" pad="39"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$40" pad="40"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="A100229CT-ND" constant="no"/>
<attribute name="NOTE" value="Cable cost not included" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.5" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.53" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="boost">
<packages>
<package name="TSOT-6">
<smd name="P$1" x="-0.95" y="-1.295" dx="1.27" dy="0.69" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="-1.295" dx="1.27" dy="0.69" layer="1" rot="R90"/>
<smd name="P$3" x="0.95" y="-1.295" dx="1.27" dy="0.69" layer="1" rot="R90"/>
<smd name="P$4" x="0.95" y="1.295" dx="1.27" dy="0.69" layer="1" rot="R90"/>
<smd name="P$5" x="0" y="1.295" dx="1.27" dy="0.69" layer="1" rot="R90"/>
<smd name="P$6" x="-0.95" y="1.295" dx="1.27" dy="0.69" layer="1" rot="R90"/>
<wire x1="-2" y1="2.5" x2="-2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-2" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2" y1="-2.5" x2="2" y2="2.5" width="0.127" layer="21"/>
<wire x1="2" y1="2.5" x2="-2" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.5" x2="1.5" y2="0.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.5" x2="1.5" y2="-0.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.5" x2="-1.5" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.5" x2="-1.5" y2="0.5" width="0.127" layer="51"/>
<circle x="-1.625" y="-2.125" radius="0.25" width="0.127" layer="21"/>
<circle x="-1.625" y="-2.125" radius="0.125" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1.905" y="0.835" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
</package>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.508" x2="1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.508" x2="-1.27" y2="0.508" width="0.127" layer="21"/>
</package>
<package name="FILM-CAPACITOR-0.47U/400V">
<wire x1="-9.25" y1="3.9" x2="9.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="3.9" x2="9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="-3.9" x2="-9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-9.25" y1="-3.9" x2="-9.25" y2="3.9" width="0.127" layer="21"/>
<pad name="P$1" x="-7.5" y="0" drill="0.9"/>
<pad name="P$2" x="7.5" y="0" drill="0.9"/>
<text x="-2.54" y="4.17" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.17" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="51"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="51"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MIC2619">
<pin name="SW" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="GND" x="0" y="-17.78" length="middle" rot="R90"/>
<pin name="FB" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="EN" x="-17.78" y="-7.62" length="middle"/>
<pin name="OVP" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="VIN" x="-17.78" y="7.62" length="middle"/>
<wire x1="-12.7" y1="10.16" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="CAPACITOR">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MIC2619" prefix="BOOST">
<gates>
<gate name="G$1" symbol="MIC2619" x="7.62" y="0"/>
</gates>
<devices>
<device name="" package="TSOT-6">
<connects>
<connect gate="G$1" pin="EN" pad="P$4"/>
<connect gate="G$1" pin="FB" pad="P$3"/>
<connect gate="G$1" pin="GND" pad="P$2"/>
<connect gate="G$1" pin="OVP" pad="P$5"/>
<connect gate="G$1" pin="SW" pad="P$1"/>
<connect gate="G$1" pin="VIN" pad="P$6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="576-3655-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="42" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.56" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0.1UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1095-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FILM_AC-CAP" package="FILM-CAPACITOR-0.47U/400V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1275-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1079-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.23"/>
</technology>
</technologies>
</device>
<device name="-33PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1055-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-18PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1052-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-DNP/NI" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2087-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3022-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-3386-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.11"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1821-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.12"/>
</technology>
</technologies>
</device>
<device name="-0.01UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1132-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4700PF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2062-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1524-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3012-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.16" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/25V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3335-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7NF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9092-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-18PF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="77-VJ0805A180GXQCBC" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.06" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/10V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1356-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.24" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-9730-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1024-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/25V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1353-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3303-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1532-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22PF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9031-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-5043-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10NF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1926-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1993-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.47" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V." package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-4112-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V-0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3340-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1494-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2200PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1085-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1281-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.022UF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1063-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1296-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BaseApp">
<packages>
<package name="C0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1.905" y="0.835" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
</package>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.508" x2="1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.508" x2="-1.27" y2="0.508" width="0.127" layer="21"/>
</package>
<package name="FILM-CAPACITOR-0.47U/400V">
<wire x1="-9.25" y1="3.9" x2="9.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="3.9" x2="9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="-3.9" x2="-9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-9.25" y1="-3.9" x2="-9.25" y2="3.9" width="0.127" layer="21"/>
<pad name="P$1" x="-7.5" y="0" drill="0.9"/>
<pad name="P$2" x="7.5" y="0" drill="0.9"/>
<text x="-2.54" y="4.17" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.17" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="51"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="51"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
</package>
<package name="SMADIODE">
<wire x1="-2.15" y1="1.3" x2="2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.3" x2="2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.3" x2="-2.15" y2="-1.3" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.3" x2="-2.15" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-3.789" y1="-1.394" x2="-3.789" y2="-1.146" width="0.127" layer="21"/>
<wire x1="-3.789" y1="-1.146" x2="-3.789" y2="1.6" width="0.127" layer="21"/>
<wire x1="-3.789" y1="1.6" x2="3.816" y2="1.6" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.6" x2="3.816" y2="1.394" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="1.3365" width="0.127" layer="21"/>
<wire x1="3.816" y1="1.394" x2="3.816" y2="-1.6" width="0.127" layer="21"/>
<wire x1="3.816" y1="-1.6" x2="-3.789" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-3.789" y1="-1.6" x2="-3.789" y2="-1.146" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="-0.4445" x2="-0.3175" y2="0.4445" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="0.4445" x2="-0.6985" y2="0" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="0" x2="-0.3175" y2="-0.4445" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="-0.4445" x2="-0.6985" y2="0.4445" width="0.127" layer="21"/>
<smd name="C" x="-2.3495" y="0" dx="2.54" dy="2.54" layer="1"/>
<smd name="A" x="2.3495" y="0" dx="2.54" dy="2.54" layer="1" rot="R180"/>
<text x="-2.54" y="1.905" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.286" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-2.825" y1="-1.1" x2="-2.175" y2="1.1" layer="51"/>
<rectangle x1="2.175" y1="-1.1" x2="2.825" y2="1.1" layer="51" rot="R180"/>
<rectangle x1="-1.75" y1="-1.225" x2="-1.075" y2="1.225" layer="51"/>
</package>
<package name="SCHOTTKY_SMA">
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.15" y="0" dx="1.27" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.15" y="0" dx="1.27" dy="1.47" layer="1"/>
<text x="-2.286" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.254" y="1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="DO-214AB">
<smd name="C" x="-3.15" y="0" dx="3" dy="2.3" layer="1"/>
<smd name="A" x="3.15" y="0" dx="3" dy="2.3" layer="1"/>
<wire x1="-4" y1="2.5" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="1.5" width="0.127" layer="21"/>
<wire x1="-4" y1="2.5" x2="-4" y2="1.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-1.5" x2="-4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-2.5" x2="4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="4" y1="-2.5" x2="4" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1" y1="2" x2="-1" y2="-2" width="0.127" layer="21"/>
<text x="-3" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TMINIP2-F2-B">
<smd name="C" x="0" y="-2.2" dx="2.5" dy="1.5" layer="1" rot="R180"/>
<smd name="A" x="0" y="2.2" dx="2.5" dy="1.5" layer="1" rot="R180"/>
<wire x1="1.5" y1="-1.9" x2="-1.5" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.9" x2="-1.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.9" x2="1.5" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.9" x2="1.5" y2="-1.9" width="0.127" layer="21"/>
<wire x1="1.2" y1="-1" x2="-1.2" y2="-1" width="0.127" layer="21"/>
<text x="2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SOD-323">
<wire x1="-0.85" y1="0.65" x2="0.85" y2="0.65" width="0.127" layer="21"/>
<wire x1="0.85" y1="0.65" x2="0.85" y2="-0.65" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.65" x2="-0.85" y2="-0.65" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-0.65" x2="-0.85" y2="0.65" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.6" x2="0.4" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.6" x2="0.3" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.3" y1="-0.6" x2="0.3" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.9" y1="0.2" x2="-1.2" y2="0.2" width="0.127" layer="51"/>
<wire x1="-1.2" y1="0.2" x2="-1.2" y2="-0.2" width="0.127" layer="51"/>
<wire x1="-1.2" y1="-0.2" x2="-0.9" y2="-0.2" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.2" x2="1.2" y2="0.2" width="0.127" layer="51"/>
<wire x1="1.2" y1="0.2" x2="1.2" y2="-0.2" width="0.127" layer="51"/>
<wire x1="1.2" y1="-0.2" x2="0.9" y2="-0.2" width="0.127" layer="51"/>
<smd name="A" x="-1" y="0" dx="1" dy="0.8" layer="1"/>
<smd name="C" x="1" y="0" dx="1" dy="0.8" layer="1"/>
<text x="-1.8" y="0.9" size="0.8128" layer="25" font="vector">&gt;NAME</text>
<text x="-2.1" y="-1.7" size="0.8128" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="CPOL_B">
<wire x1="-2.1" y1="2.1" x2="1" y2="2.1" width="0.1016" layer="51"/>
<wire x1="1" y1="2.1" x2="2.1" y2="1" width="0.1016" layer="51"/>
<wire x1="2.1" y1="1" x2="2.1" y2="-1" width="0.1016" layer="51"/>
<wire x1="2.1" y1="-1" x2="1" y2="-2.1" width="0.1016" layer="51"/>
<wire x1="1" y1="-2.1" x2="-2.1" y2="-2.1" width="0.1016" layer="51"/>
<wire x1="-2.1" y1="-2.1" x2="-2.1" y2="2.1" width="0.1016" layer="51"/>
<wire x1="-1.75" y1="0.85" x2="1.75" y2="0.85" width="0.1016" layer="21" curve="-128.186984"/>
<wire x1="-1.75" y1="-0.85" x2="1.75" y2="-0.85" width="0.1016" layer="21" curve="128.186984"/>
<wire x1="-2.1" y1="0.85" x2="-2.1" y2="2.1" width="0.1016" layer="21"/>
<wire x1="-2.1" y1="2.1" x2="1" y2="2.1" width="0.1016" layer="21"/>
<wire x1="1" y1="2.1" x2="2.1" y2="1" width="0.1016" layer="21"/>
<wire x1="2.1" y1="-1" x2="1" y2="-2.1" width="0.1016" layer="21"/>
<wire x1="1" y1="-2.1" x2="-2.1" y2="-2.1" width="0.1016" layer="21"/>
<wire x1="-2.1" y1="-2.1" x2="-2.1" y2="-0.85" width="0.1016" layer="21"/>
<wire x1="-1.2" y1="1.5" x2="-1.2" y2="-1.5" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="1.95" width="0.1016" layer="51"/>
<smd name="-" x="-1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<smd name="+" x="1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<text x="-2.15" y="2.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.15" y="-3.275" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3" y1="-0.35" x2="-1.85" y2="0.35" layer="51"/>
<rectangle x1="1.9" y1="-0.35" x2="2.3" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-1.25" y="1.45"/>
<vertex x="-1.7" y="0.85"/>
<vertex x="-1.85" y="0.35"/>
<vertex x="-1.85" y="-0.4"/>
<vertex x="-1.7" y="-0.85"/>
<vertex x="-1.25" y="-1.4"/>
<vertex x="-1.25" y="1.4"/>
</polygon>
</package>
<package name="CPOL_D">
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="0.95" x2="-3.25" y2="3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="0.95" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-0.95" x2="3.25" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="21"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="-" x="-2.4" y="0" dx="3" dy="1.4" layer="1"/>
<smd name="+" x="2.4" y="0" dx="3" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="CPOL-G">
<wire x1="-5" y1="5" x2="3.23" y2="5" width="0.1016" layer="51"/>
<wire x1="3.23" y1="5" x2="4.79" y2="1.55" width="0.1016" layer="51"/>
<wire x1="4.79" y1="1.55" x2="4.79" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="4.79" y1="-1.55" x2="3.23" y2="-5" width="0.1016" layer="51"/>
<wire x1="3.23" y1="-5" x2="-5" y2="-5" width="0.1016" layer="51"/>
<wire x1="-5" y1="-5" x2="-5" y2="5" width="0.1016" layer="51"/>
<wire x1="-5" y1="0.95" x2="-5" y2="5" width="0.1016" layer="21"/>
<wire x1="-5" y1="5" x2="3.23" y2="5" width="0.1016" layer="21"/>
<wire x1="3.23" y1="5" x2="4.79" y2="1.55" width="0.1016" layer="21"/>
<wire x1="4.79" y1="1.55" x2="4.79" y2="0.95" width="0.1016" layer="21"/>
<wire x1="4.79" y1="1.59" x2="4.79" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="4.79" y1="-1.55" x2="3.23" y2="-5" width="0.1016" layer="21"/>
<wire x1="3.23" y1="-5" x2="-5" y2="-5" width="0.1016" layer="21"/>
<wire x1="-5" y1="-5" x2="-5" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="-" x="-3.65" y="0" dx="3.2" dy="1.4" layer="1"/>
<smd name="+" x="3.65" y="0" dx="3.2" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="CPOL_F_8X10">
<wire x1="-4.1" y1="5.1" x2="1.5" y2="5.1" width="0.1016" layer="51"/>
<wire x1="1.5" y1="5.1" x2="4.1" y2="2.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="2.8" x2="4.1" y2="-2.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="-2.8" x2="1.3" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="1.3" y1="-5.1" x2="-4.1" y2="-5.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="-5.1" x2="-4.1" y2="5.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="1" x2="-4.1" y2="5.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="5.1" x2="1.5" y2="5.1" width="0.1016" layer="21"/>
<wire x1="1.5" y1="5.1" x2="4.1" y2="2.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="2.8" x2="4.1" y2="1" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-1" x2="4.1" y2="-2.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-2.8" x2="1.3" y2="-5.1" width="0.1016" layer="21"/>
<wire x1="1.3" y1="-5.1" x2="-4.1" y2="-5.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="-5.1" x2="-4.1" y2="-1" width="0.1016" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.1016" layer="21" curve="-153.684915"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.1016" layer="21" curve="153.684915"/>
<circle x="0" y="0" radius="4" width="0.001" layer="51"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3.55" y="0" dx="4" dy="1.6" layer="1"/>
<smd name="+" x="3.55" y="0" dx="4" dy="1.6" layer="1"/>
<text x="-1.75" y="1.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-2.375" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-4.85" y1="-0.45" x2="-3.9" y2="0.45" layer="51"/>
<rectangle x1="3.9" y1="-0.45" x2="4.85" y2="0.45" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="CPOL_E">
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="4.1" width="0.1016" layer="51"/>
<wire x1="-4.1" y1="0.9" x2="-4.1" y2="4.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="4.1" x2="1.8" y2="4.1" width="0.1016" layer="21"/>
<wire x1="1.8" y1="4.1" x2="4.1" y2="1.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="1.8" x2="4.1" y2="0.9" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-0.9" x2="4.1" y2="-1.8" width="0.1016" layer="21"/>
<wire x1="4.1" y1="-1.8" x2="1.8" y2="-4.1" width="0.1016" layer="21"/>
<wire x1="1.8" y1="-4.1" x2="-4.1" y2="-4.1" width="0.1016" layer="21"/>
<wire x1="-4.1" y1="-4.1" x2="-4.1" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-2.2" y1="3.25" x2="-2.2" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.85" y1="0.9" x2="3.85" y2="0.9" width="0.1016" layer="21" curve="-153.684915"/>
<wire x1="-3.85" y1="-0.9" x2="3.85" y2="-0.9" width="0.1016" layer="21" curve="153.684915"/>
<circle x="0" y="0" radius="3.95" width="0.1016" layer="51"/>
<smd name="-" x="-3" y="0" dx="3.8" dy="1.4" layer="1"/>
<smd name="+" x="3" y="0" dx="3.8" dy="1.4" layer="1"/>
<text x="-1.8" y="1.3" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.8" y="-2.225" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5" y1="-0.35" x2="-3.8" y2="0.35" layer="51"/>
<rectangle x1="3.8" y1="-0.35" x2="4.5" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.25" y="3.2"/>
<vertex x="-3" y="2.5"/>
<vertex x="-3.6" y="1.5"/>
<vertex x="-3.85" y="0.65"/>
<vertex x="-3.85" y="-0.65"/>
<vertex x="-3.55" y="-1.6"/>
<vertex x="-2.95" y="-2.55"/>
<vertex x="-2.25" y="-3.2"/>
<vertex x="-2.25" y="3.15"/>
</polygon>
</package>
<package name="CPOL-16X16">
<wire x1="-7.54" y1="8.5" x2="5.77" y2="8.5" width="0.1016" layer="51"/>
<wire x1="5.77" y1="8.5" x2="7.33" y2="6" width="0.1016" layer="51"/>
<wire x1="7.33" y1="6" x2="7.33" y2="-6" width="0.1016" layer="51"/>
<wire x1="7.33" y1="-6" x2="5.77" y2="-8.5" width="0.1016" layer="51"/>
<wire x1="5.77" y1="-8.5" x2="-7.54" y2="-8.5" width="0.1016" layer="51"/>
<wire x1="-7.54" y1="-8.5" x2="-7.54" y2="8.5" width="0.1016" layer="51"/>
<wire x1="-7.54" y1="0.95" x2="-7.54" y2="8.5" width="0.1016" layer="21"/>
<wire x1="-7.54" y1="8.5" x2="5.77" y2="8.5" width="0.1016" layer="21"/>
<wire x1="5.77" y1="8.5" x2="7.33" y2="6" width="0.1016" layer="21"/>
<wire x1="7.33" y1="6" x2="7.33" y2="-6" width="0.1016" layer="21"/>
<wire x1="7.33" y1="-6" x2="5.77" y2="-8.5" width="0.1016" layer="21"/>
<wire x1="5.77" y1="-8.5" x2="-7.54" y2="-8.5" width="0.1016" layer="21"/>
<wire x1="-7.54" y1="-8.5" x2="-7.54" y2="-0.95" width="0.1016" layer="21"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.95" width="0.1016" layer="21" curve="-144.299363"/>
<wire x1="-2.95" y1="-0.95" x2="2.95" y2="-0.95" width="0.1016" layer="21" curve="144.299363"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="-" x="-5.4" y="0" dx="5.4" dy="1.4" layer="1"/>
<smd name="+" x="5.4" y="0" dx="5.4" dy="1.4" layer="1"/>
<text x="-1.75" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.75" y="-1.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
</package>
<package name="EIA3528-KIT">
<wire x1="-0.9" y1="-1.6" x2="-3.1" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.6" x2="-3.1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.55" x2="2.7" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.55" x2="3.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.1" y1="-1.2" x2="3.1" y2="1.25" width="0.2032" layer="21"/>
<wire x1="3.1" y1="1.25" x2="2.7" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="0.609" y1="1.311" x2="0.609" y2="-1.286" width="0.4" layer="21" style="longdash"/>
<smd name="C" x="-1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<smd name="A" x="1.9" y="0" dx="1.7" dy="2.5" layer="1"/>
<text x="-2.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.24" y="-1.37" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="E5-10.5">
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="5.08" width="0.1524" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="4.699" y="2.7432" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.1242" y="-3.2258" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
</package>
<package name="E5-12.5">
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="6.5" width="0.1524" layer="21"/>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="4.699" y="2.7432" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.1242" y="-3.2258" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
</package>
<package name="EIA7343">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.2032" layer="21"/>
</package>
<package name="C2917">
<wire x1="-5" y1="2.5" x2="-2" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="2.5" x2="-5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2.5" x2="-2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5" y1="-1.5" x2="4" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="21"/>
<smd name="C" x="-3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<smd name="A" x="3.17" y="0" dx="2.55" dy="2.7" layer="1" rot="R180"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.27" size="0.4064" layer="27">&gt;Value</text>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.2032" layer="21"/>
</package>
<package name="EIA3216">
<wire x1="-1" y1="-1.2" x2="-2.5" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.2" x2="-2.5" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.2" x2="-1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.2" x2="2.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-1.2" x2="2.5" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-0.8" x2="2.5" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.5" y1="0.8" x2="2.1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.1" y1="1.2" x2="1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="-1.016" width="0.127" layer="21"/>
<smd name="C" x="-1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<smd name="A" x="1.4" y="0" dx="1.6" dy="1.4" layer="1" rot="R90"/>
<text x="-2.54" y="1.381" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.408" y="1.332" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="C1206-POL">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="-0.254" y1="1.016" x2="-0.254" y2="-1.016" width="0.127" layer="21"/>
</package>
<package name="R0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-2.032" y="0.635" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.032" y="-2.159" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.63" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.63" width="0.127" layer="21"/>
</package>
<package name="R-0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.3" y1="0.473" x2="-1.3" y2="-0.473" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-0.473" x2="1.2" y2="-0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.473" x2="1.2" y2="0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.473" x2="-1.3" y2="0.473" width="0.127" layer="21"/>
</package>
<package name="10555-METRIC">
<smd name="P$1" x="-4.85" y="0" dx="3.3" dy="2" layer="1"/>
<smd name="P$2" x="4.35" y="0" dx="3.3" dy="2" layer="1"/>
<wire x1="-6.75" y1="3" x2="-6.75" y2="-3" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-3" x2="6.25" y2="-3" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3" x2="6.25" y2="3" width="0.127" layer="21"/>
<wire x1="6.25" y1="3" x2="-6.75" y2="3" width="0.127" layer="21"/>
<text x="-2.75" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805-METRIC">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
</package>
<package name="E2-5">
<wire x1="-1.524" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.778" x2="-0.762" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="1.524" x2="-1.016" y2="2.032" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="-" x="1.016" y="0" drill="0.8128" diameter="1.27" shape="octagon"/>
<pad name="+" x="-1.016" y="0" drill="0.8128" diameter="1.27"/>
<text x="2.54" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.54" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="51"/>
</package>
<package name="E3.5-8">
<wire x1="-3.429" y1="1.143" x2="-2.667" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0.762" x2="-3.048" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="4.064" width="0.1524" layer="21"/>
<pad name="-" x="1.778" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.778" y="0" drill="0.8128" diameter="1.6002"/>
<text x="3.302" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="E2.5-6">
<wire x1="-2.159" y1="0" x2="-2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.254" x2="-2.413" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1524" layer="21"/>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.5748" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.5748"/>
<text x="2.667" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.667" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="R0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="-0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="-2" y2="0.75" width="0.127" layer="21"/>
</package>
<package name="R2512">
<smd name="P$1" x="0" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<smd name="P$2" x="6.5024" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<wire x1="-1.27" y1="1.9304" x2="-1.27" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.9304" x2="7.7724" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="-1.9304" x2="7.7724" y2="1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="1.9304" x2="-1.27" y2="1.9304" width="0.127" layer="21"/>
<text x="1.016" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0.508" y="-3.556" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="5.334" y1="1.524" x2="1.016" y2="1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="-1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="-1.524" x2="5.334" y2="-1.524" width="0.127" layer="51"/>
<wire x1="5.334" y1="-1.524" x2="5.334" y2="1.524" width="0.127" layer="51"/>
</package>
<package name="MELF0204">
<smd name="P$1" x="-0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.8" x2="1.2" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.8" x2="1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.8" x2="-1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.8" x2="-1.2" y2="0.8" width="0.127" layer="21"/>
<text x="-0.8" y="1.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.8" y="-2.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="PIHEAD-1X2">
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="PINHEAD-1X2-3.96MM">
<wire x1="-3.9" y1="1.27" x2="3.9" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.9" y1="-1.27" x2="-3.9" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.9" y1="-1.27" x2="-3.9" y2="1.27" width="0.127" layer="21"/>
<pad name="1" x="-1.98" y="0" drill="1.75" diameter="3" shape="octagon" rot="R90"/>
<pad name="2" x="1.98" y="0" drill="1.75" diameter="3" shape="octagon" rot="R90"/>
<wire x1="3.9" y1="1.27" x2="3.9" y2="-1.27" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-3.302" y1="0" x2="-3.302" y2="5.41" width="0.127" layer="51"/>
<wire x1="-3.302" y1="5.41" x2="3.302" y2="5.41" width="0.127" layer="51"/>
<wire x1="3.302" y1="5.41" x2="3.302" y2="0" width="0.127" layer="51"/>
<wire x1="-3.302" y1="5.41" x2="-3.302" y2="8.71" width="0.127" layer="21"/>
<wire x1="-3.302" y1="8.71" x2="3.302" y2="8.71" width="0.127" layer="21"/>
<wire x1="3.302" y1="8.71" x2="3.302" y2="5.41" width="0.127" layer="21"/>
<wire x1="-3.302" y1="8.71" x2="-3.302" y2="20.14" width="0.127" layer="51"/>
<wire x1="-3.302" y1="20.14" x2="3.302" y2="20.14" width="0.127" layer="51"/>
<wire x1="3.302" y1="20.14" x2="3.302" y2="8.71" width="0.127" layer="51"/>
<wire x1="-3.302" y1="0" x2="3.302" y2="0" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="CAPACITOR">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<pin name="GND" x="0" y="0" visible="pad" length="short" direction="sup" rot="R270"/>
<text x="2.54" y="0" size="1.27" layer="95" font="vector" rot="R90">GND</text>
</symbol>
<symbol name="SCHOTTKY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CPOL">
<wire x1="-1.524" y1="1.651" x2="1.524" y2="1.651" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.651" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.54" x2="-1.524" y2="1.651" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.54" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<text x="1.143" y="3.0226" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="2.9464" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-2.0574" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0" x2="1.651" y2="0.889" layer="94"/>
<pin name="-" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="RESISTOR">
<text x="-2.54" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-2.54" y="-3.81" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="PINHEAD-1X2">
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<wire x1="1.27" y1="-2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0.1UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1095-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FILM_AC-CAP" package="FILM-CAPACITOR-0.47U/400V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1275-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1079-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.23"/>
</technology>
</technologies>
</device>
<device name="-33PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1055-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-18PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1052-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-DNP/NI" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2087-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3022-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-3386-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.11"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1821-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.12"/>
</technology>
</technologies>
</device>
<device name="-0.01UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1132-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4700PF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2062-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1524-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3012-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.16" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/25V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3335-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7NF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9092-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-18PF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="77-VJ0805A180GXQCBC" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.06" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/10V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1356-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.24" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-9730-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1024-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/25V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1353-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3303-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1532-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22PF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9031-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-5043-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10NF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1926-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1993-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.47" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V." package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-4112-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V-0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3340-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1494-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2200PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1085-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1281-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.022UF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1063-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1296-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/50V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1909-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0367" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SCHOTTKY-DIODE" prefix="SCHOTTKY" uservalue="yes">
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="SMADIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="SS14-E3/5ATGICT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.45" constant="no"/>
</technology>
</technologies>
</device>
<device name="SS14-40V/1A" package="SCHOTTKY_SMA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="SS14-E3/61TGICT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.286" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="DO-214AB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-40V/5A" package="TMINIP2-F2-B">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="DB2441700LCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.65"/>
</technology>
</technologies>
</device>
<device name="-PMEG3010-30V_1A" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="568-6511-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.43" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CPOL" prefix="CPOL" uservalue="yes">
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="-2.54"/>
</gates>
<devices>
<device name="_B" package="CPOL_B">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_D" package="CPOL_D">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="CPOL-G">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-F" package="CPOL_F_8X10">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-E" package="CPOL_E">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-16X16" package="CPOL-16X16">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-TANTALUM-100UF/6.3V" package="EIA3528-KIT">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="478-8156-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.61"/>
</technology>
</technologies>
</device>
<device name="-470UF/25V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1189-2584-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.56"/>
</technology>
</technologies>
</device>
<device name="-680UF/35V-0.041OHM" package="E5-12.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="495-6062-ND"/>
<attribute name="PRICE_PER_UNIT" value="1.77"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM_100UF/10V/0.08OHM" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-8497-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="1.49"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM_330UF/6.3V/0.04OHM" package="EIA7343">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-10400-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.69"/>
</technology>
</technologies>
</device>
<device name="-1000UF/10V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="493-10963-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.4"/>
</technology>
</technologies>
</device>
<device name="-220UF/35V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6109-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.32" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470UF/35V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6592-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.43" constant="no"/>
</technology>
</technologies>
</device>
<device name="-330UF/35V" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="493-1083-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.37" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680UF/35V-19MM" package="E5-10.5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="565-1579-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.63" constant="no"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM_220UF/16V" package="C2917">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-10429-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM-10UF/6.3V-1206" package="EIA3216">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="495-2181-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.29" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100UF/6.3V" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6602-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-220UF/25V" package="E3.5-8">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6567-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
<device name="-220UF/10V" package="E2.5-6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6565-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150UF/10V" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1189-1879-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.25" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100UF/10V" package="E2-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-6600-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.0509" constant="no"/>
</technology>
</technologies>
</device>
<device name="-TANTALUM-10UF/10V" package="C1206-POL">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="718-1118-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.128" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.31" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;B&gt;Resistance&lt;/B&gt; &lt;p&gt; 
Mount :-Surface mount</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-50R_1/8W_RF" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0603-50-E3 "/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-50R_1/20W_RF" package="R-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="FC0402-50BWCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.52"/>
</technology>
</technologies>
</device>
<device name="-POWER_RESISTOR" package="10555-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-100K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-100K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-1M_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00M-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-12K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-12.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-330R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF0603JT330RCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-30R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P30.0HCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0R" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P0.0GCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-4.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-24K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM24KCFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-560R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-560GRDKR-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.7K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-2.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-33R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM33.0CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-6.2K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM6.2KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-10.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-10R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P10.0HCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5K_1/8W" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0805-5K" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300K_/1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM300KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-33K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-33.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-110K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-110KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-20K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-20.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-140K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-140KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-84.5K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-84.5KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4823-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15K_1/8W" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-15.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-47.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_0.125W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-10KARCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/4W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM1.0KDCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-698R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-698HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.5K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.50K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-3.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.58K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RC0603FR-071K58L" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-51.1R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-51.1HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.37K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.37KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-150GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-82GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-130R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-130GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12.1K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-12.1KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-270KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-100HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM470AZCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.13" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.0003R/3W-CURRENTSENSE" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="HCS2512FTL300CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.9625" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6.8R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-6.80ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.102" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-470ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-270KACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-680GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0048" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="267K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-267KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-191K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4862-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" prefix="FID" uservalue="yes">
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No_Part. Plain Copper Pads."/>
<attribute name="PRICE_PER_UNIT" value="0"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD-1X2">
<gates>
<gate name="G$1" symbol="PINHEAD-1X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIHEAD-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MOLEX_1X2-3.96MM" package="PINHEAD-1X2-3.96MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="WM4640-ND &amp; WM2122-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.52" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="inductor">
<packages>
<package name="INDUCTOR-2.4X2.4X1.2">
<smd name="P$1" x="0" y="0" dx="2.5" dy="1" layer="1" rot="R90"/>
<smd name="P$2" x="1.45" y="0" dx="2.5" dy="1" layer="1" rot="R90"/>
<wire x1="-0.95" y1="1.45" x2="2.4" y2="1.45" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.45" x2="2.4" y2="-1.45" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.45" x2="-0.95" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-0.95" y1="-1.45" x2="-0.95" y2="1.45" width="0.127" layer="21"/>
<text x="-2.5" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="INDUCTOR-3X2.8">
<smd name="P$1" x="-1.1" y="0" dx="2.5" dy="1" layer="1" rot="R90"/>
<smd name="P$2" x="1.1" y="0" dx="2.5" dy="1" layer="1" rot="R90"/>
<wire x1="-1.680003125" y1="1.45" x2="1.669996875" y2="1.45" width="0.127" layer="21"/>
<wire x1="1.669996875" y1="1.45" x2="1.669996875" y2="-1.45" width="0.127" layer="21"/>
<wire x1="1.669996875" y1="-1.45" x2="-1.680003125" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-1.680003125" y1="-1.45" x2="-1.680003125" y2="1.45" width="0.127" layer="21"/>
<text x="-3.77" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.77" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="INDUCTOR">
<pin name="P$1" x="-12.7" y="0" length="middle"/>
<pin name="P$2" x="12.7" y="0" length="middle" rot="R180"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-5.08" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="INDUCTOR" prefix="L">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-10UH/0.45A" package="INDUCTOR-2.4X2.4X1.2">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-10UH/0.7MA" package="INDUCTOR-3X2.8">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-5340-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.2551" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.39" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_L">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Passives">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="PAD.02X.02">
<smd name="P$1" x="0" y="0" dx="0.508" dy="0.508" layer="1"/>
</package>
<package name="PAD.03X.03">
<smd name="P$1" x="0" y="0" dx="0.762" dy="0.762" layer="1" roundness="100" cream="no"/>
</package>
<package name="PAD.03X.05">
<smd name="P$1" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
</package>
<package name="PAD.03X.04">
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100" cream="no"/>
</package>
<package name="TP_15TH">
<pad name="P$1" x="0" y="0" drill="0.381" diameter="0.6096" stop="no"/>
<circle x="0" y="0" radius="0.381" width="0" layer="30"/>
</package>
</packages>
<symbols>
<symbol name="TEST-POINT">
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.762" x2="3.302" y2="-0.762" width="0.1524" layer="94" curve="180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;Name</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;Value</text>
<pin name="1" x="0" y="0" visible="off" length="point" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEST-POINT" prefix="TP">
<description>Bare copper test points for troubleshooting or ICT</description>
<gates>
<gate name="G$1" symbol="TEST-POINT" x="0" y="0"/>
</gates>
<devices>
<device name="2" package="PAD.02X.02">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3" package="PAD.03X.03">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3X5" package="PAD.03X.05">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3X4" package="PAD.03X.04">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_15TH_THRU" package="TP_15TH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="BOOST" library="boost" deviceset="MIC2619" device=""/>
<part name="C1" library="BaseApp" deviceset="CAPACITOR" device="-1UF/10V" value="1UF"/>
<part name="GND1" library="BaseApp" deviceset="GND" device=""/>
<part name="L1" library="inductor" deviceset="INDUCTOR" device="-10UH/0.7MA" value="INDUCTOR-10UH/0.7MA"/>
<part name="SS14" library="BaseApp" deviceset="SCHOTTKY-DIODE" device="SS14-40V/1A"/>
<part name="C3" library="boost" deviceset="CAPACITOR" device="-22NF/50V" value="22NF/50V"/>
<part name="CPOL1" library="BaseApp" deviceset="CPOL" device="-TANTALUM-10UF/10V" value="10UF"/>
<part name="R2" library="BaseApp" deviceset="RESISTOR" device="267K" value="267K*"/>
<part name="R1" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="R4" library="BaseApp" deviceset="RESISTOR" device="-191K" value="191K"/>
<part name="R3" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="GND2" library="BaseApp" deviceset="GND" device=""/>
<part name="C2" library="BaseApp" deviceset="CAPACITOR" device="-1UF/50V" value="1UF/50V"/>
<part name="LCD-BOARD-SIDE" library="connectors" deviceset="FFC-40PIN" device=""/>
<part name="GND3" library="BaseApp" deviceset="GND" device=""/>
<part name="GND4" library="BaseApp" deviceset="GND" device=""/>
<part name="GND5" library="BaseApp" deviceset="GND" device=""/>
<part name="GND6" library="BaseApp" deviceset="GND" device=""/>
<part name="FID1" library="BaseApp" deviceset="FIDUCIAL" device=""/>
<part name="FID2" library="BaseApp" deviceset="FIDUCIAL" device=""/>
<part name="FID3" library="BaseApp" deviceset="FIDUCIAL" device=""/>
<part name="FID4" library="BaseApp" deviceset="FIDUCIAL" device=""/>
<part name="FRAME2" library="frames" deviceset="DINA4_L" device=""/>
<part name="TP1" library="SparkFun-Passives" deviceset="TEST-POINT" device="3X5" value="VIN"/>
<part name="TP2" library="SparkFun-Passives" deviceset="TEST-POINT" device="3X5" value="25V"/>
<part name="TP3" library="SparkFun-Passives" deviceset="TEST-POINT" device="3X5" value="GND"/>
<part name="R5" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="GND7" library="BaseApp" deviceset="GND" device=""/>
<part name="MAIN-BOARD-SIDE1" library="connectors" deviceset="FFC-40PIN" device=""/>
<part name="GND8" library="BaseApp" deviceset="GND" device=""/>
<part name="GND9" library="BaseApp" deviceset="GND" device=""/>
<part name="GND10" library="BaseApp" deviceset="GND" device=""/>
<part name="GND11" library="BaseApp" deviceset="GND" device=""/>
<part name="GND12" library="BaseApp" deviceset="GND" device=""/>
<part name="GND13" library="BaseApp" deviceset="GND" device=""/>
<part name="U$1" library="BaseApp" deviceset="PINHEAD-1X2" device=""/>
<part name="GND14" library="BaseApp" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="0" y1="139.7" x2="144.78" y2="139.7" width="1.016" layer="94"/>
<wire x1="144.78" y1="139.7" x2="254" y2="139.7" width="1.016" layer="94"/>
<wire x1="254" y1="139.7" x2="254" y2="73.66" width="1.016" layer="94"/>
<wire x1="254" y1="73.66" x2="254" y2="38.1" width="1.016" layer="94"/>
<wire x1="254" y1="38.1" x2="254" y2="0" width="1.016" layer="94"/>
<wire x1="254" y1="0" x2="144.78" y2="0" width="1.016" layer="94"/>
<wire x1="144.78" y1="0" x2="0" y2="0" width="1.016" layer="94"/>
<wire x1="0" y1="139.7" x2="0" y2="0" width="1.016" layer="94"/>
<wire x1="144.78" y1="139.7" x2="144.78" y2="73.66" width="1.016" layer="94" style="shortdash"/>
<wire x1="144.78" y1="73.66" x2="144.78" y2="38.1" width="1.016" layer="94" style="shortdash"/>
<wire x1="144.78" y1="38.1" x2="144.78" y2="0" width="1.016" layer="94" style="shortdash"/>
<wire x1="144.78" y1="73.66" x2="254" y2="73.66" width="1.016" layer="94" style="shortdash"/>
<text x="33.02" y="5.08" size="5.08" layer="94" font="vector" ratio="15">FFC CONNECTORS</text>
<text x="167.64" y="73.66" size="5.08" layer="94" ratio="15">3.3V-25V BOOST CIRCUIT</text>
<wire x1="144.78" y1="38.1" x2="254" y2="38.1" width="1.016" layer="94" style="shortdash"/>
<text x="162.56" y="40.64" size="5.08" layer="94" ratio="15">TEST PADS &amp; FIDUCIALS</text>
<text x="86.36" y="144.78" size="5.08" layer="94" ratio="15">TEST PADS &amp; FIDUCIALS</text>
</plain>
<instances>
<instance part="BOOST" gate="G$1" x="190.5" y="106.68"/>
<instance part="C1" gate="G$1" x="149.86" y="116.84"/>
<instance part="GND1" gate="G$1" x="154.94" y="111.76"/>
<instance part="L1" gate="G$1" x="187.96" y="129.54"/>
<instance part="SS14" gate="G$1" x="215.9" y="114.3"/>
<instance part="C3" gate="G$1" x="231.14" y="109.22"/>
<instance part="CPOL1" gate="G$1" x="160.02" y="116.84"/>
<instance part="R2" gate="G$1" x="223.52" y="109.22" rot="R90"/>
<instance part="R1" gate="G$1" x="223.52" y="96.52" rot="R90"/>
<instance part="R4" gate="G$1" x="238.76" y="109.22" rot="R90"/>
<instance part="R3" gate="G$1" x="238.76" y="88.9" rot="R90"/>
<instance part="GND2" gate="G$1" x="190.5" y="83.82"/>
<instance part="C2" gate="G$1" x="246.38" y="106.68"/>
<instance part="LCD-BOARD-SIDE" gate="G$1" x="35.56" y="76.2"/>
<instance part="GND3" gate="G$1" x="76.2" y="114.3"/>
<instance part="GND4" gate="G$1" x="139.7" y="104.14"/>
<instance part="GND5" gate="G$1" x="5.08" y="104.14"/>
<instance part="GND6" gate="G$1" x="205.74" y="53.34"/>
<instance part="FID1" gate="G$1" x="218.44" y="55.88"/>
<instance part="FID2" gate="G$1" x="226.06" y="55.88"/>
<instance part="FID3" gate="G$1" x="233.68" y="55.88"/>
<instance part="FID4" gate="G$1" x="243.84" y="55.88"/>
<instance part="FRAME2" gate="G$2" x="152.4" y="0"/>
<instance part="TP1" gate="G$1" x="167.64" y="58.42" rot="R270"/>
<instance part="TP2" gate="G$1" x="182.88" y="58.42" rot="R270"/>
<instance part="TP3" gate="G$1" x="198.12" y="58.42" rot="R270"/>
<instance part="R5" gate="G$1" x="167.64" y="91.44" rot="R90"/>
<instance part="GND7" gate="G$1" x="167.64" y="83.82"/>
<instance part="MAIN-BOARD-SIDE1" gate="G$1" x="106.68" y="76.2"/>
<instance part="GND8" gate="G$1" x="71.12" y="99.06"/>
<instance part="GND9" gate="G$1" x="71.12" y="58.42"/>
<instance part="GND10" gate="G$1" x="139.7" y="25.4"/>
<instance part="GND11" gate="G$1" x="7.62" y="22.86"/>
<instance part="GND12" gate="G$1" x="142.24" y="68.58"/>
<instance part="GND13" gate="G$1" x="2.54" y="68.58"/>
<instance part="U$1" gate="G$1" x="152.4" y="50.8" rot="R270"/>
<instance part="GND14" gate="G$1" x="160.02" y="45.72"/>
</instances>
<busses>
</busses>
<nets>
<net name="VIN" class="0">
<segment>
<pinref part="BOOST" gate="G$1" pin="VIN"/>
<wire x1="172.72" y1="114.3" x2="170.18" y2="114.3" width="0.1524" layer="91"/>
<wire x1="170.18" y1="114.3" x2="170.18" y2="124.46" width="0.1524" layer="91"/>
<wire x1="170.18" y1="124.46" x2="170.18" y2="129.54" width="0.1524" layer="91"/>
<wire x1="160.02" y1="121.92" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
<wire x1="160.02" y1="124.46" x2="170.18" y2="124.46" width="0.1524" layer="91"/>
<junction x="170.18" y="124.46"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="149.86" y1="121.92" x2="149.86" y2="124.46" width="0.1524" layer="91"/>
<wire x1="149.86" y1="124.46" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
<junction x="160.02" y="124.46"/>
<pinref part="L1" gate="G$1" pin="P$1"/>
<wire x1="170.18" y1="129.54" x2="175.26" y2="129.54" width="0.1524" layer="91"/>
<pinref part="CPOL1" gate="G$1" pin="+"/>
<wire x1="170.18" y1="129.54" x2="170.18" y2="137.16" width="0.1524" layer="91"/>
<junction x="170.18" y="129.54"/>
<label x="170.18" y="137.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="167.64" y1="58.42" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<label x="167.64" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="TP1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$37"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$4"/>
<label x="68.58" y="134.62" size="1.778" layer="95" xref="yes"/>
<wire x1="58.42" y1="109.22" x2="68.58" y2="109.22" width="0.1524" layer="91"/>
<wire x1="68.58" y1="109.22" x2="83.82" y2="109.22" width="0.1524" layer="91"/>
<wire x1="68.58" y1="134.62" x2="68.58" y2="109.22" width="0.1524" layer="91"/>
<junction x="68.58" y="109.22"/>
</segment>
<segment>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$35"/>
<wire x1="129.54" y1="99.06" x2="137.16" y2="99.06" width="0.1524" layer="91"/>
<label x="137.16" y="99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="2"/>
<wire x1="152.4" y1="53.34" x2="152.4" y2="60.96" width="0.1524" layer="91"/>
<label x="152.4" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="149.86" y1="114.3" x2="149.86" y2="111.76" width="0.1524" layer="91"/>
<wire x1="149.86" y1="111.76" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
<wire x1="154.94" y1="111.76" x2="160.02" y2="111.76" width="0.1524" layer="91"/>
<wire x1="160.02" y1="111.76" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<pinref part="GND1" gate="G$1" pin="GND"/>
<junction x="154.94" y="111.76"/>
<pinref part="CPOL1" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="223.52" y1="91.44" x2="223.52" y2="83.82" width="0.1524" layer="91"/>
<wire x1="223.52" y1="83.82" x2="238.76" y2="83.82" width="0.1524" layer="91"/>
<pinref part="BOOST" gate="G$1" pin="GND"/>
<wire x1="190.5" y1="88.9" x2="190.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="190.5" y1="83.82" x2="223.52" y2="83.82" width="0.1524" layer="91"/>
<junction x="223.52" y="83.82"/>
<pinref part="GND2" gate="G$1" pin="GND"/>
<junction x="190.5" y="83.82"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="246.38" y1="104.14" x2="246.38" y2="83.82" width="0.1524" layer="91"/>
<wire x1="246.38" y1="83.82" x2="238.76" y2="83.82" width="0.1524" layer="91"/>
<junction x="238.76" y="83.82"/>
</segment>
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$38"/>
<wire x1="58.42" y1="114.3" x2="76.2" y2="114.3" width="0.1524" layer="91"/>
<pinref part="GND3" gate="G$1" pin="GND"/>
<wire x1="76.2" y1="114.3" x2="83.82" y2="114.3" width="0.1524" layer="91"/>
<junction x="76.2" y="114.3"/>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$40"/>
<wire x1="58.42" y1="124.46" x2="76.2" y2="124.46" width="0.1524" layer="91"/>
<wire x1="76.2" y1="124.46" x2="83.82" y2="124.46" width="0.1524" layer="91"/>
<wire x1="76.2" y1="124.46" x2="76.2" y2="114.3" width="0.1524" layer="91"/>
<junction x="76.2" y="124.46"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$1"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$3"/>
</segment>
<segment>
<wire x1="129.54" y1="104.14" x2="139.7" y2="104.14" width="0.1524" layer="91"/>
<pinref part="GND4" gate="G$1" pin="GND"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$36"/>
</segment>
<segment>
<pinref part="GND5" gate="G$1" pin="GND"/>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$5"/>
<wire x1="5.08" y1="104.14" x2="12.7" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="198.12" y1="58.42" x2="205.74" y2="58.42" width="0.1524" layer="91"/>
<wire x1="205.74" y1="58.42" x2="205.74" y2="53.34" width="0.1524" layer="91"/>
<pinref part="GND6" gate="G$1" pin="GND"/>
<pinref part="TP3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND7" gate="G$1" pin="GND"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="167.64" y1="83.82" x2="167.64" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$35"/>
<wire x1="58.42" y1="99.06" x2="71.12" y2="99.06" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$6"/>
<pinref part="GND8" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="99.06" x2="83.82" y2="99.06" width="0.1524" layer="91"/>
<junction x="71.12" y="99.06"/>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$36"/>
<wire x1="58.42" y1="104.14" x2="71.12" y2="104.14" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$5"/>
<wire x1="71.12" y1="104.14" x2="83.82" y2="104.14" width="0.1524" layer="91"/>
<wire x1="71.12" y1="104.14" x2="71.12" y2="99.06" width="0.1524" layer="91"/>
<junction x="71.12" y="104.14"/>
</segment>
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$27"/>
<wire x1="58.42" y1="58.42" x2="71.12" y2="58.42" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$14"/>
<pinref part="GND9" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="58.42" x2="83.82" y2="58.42" width="0.1524" layer="91"/>
<junction x="71.12" y="58.42"/>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$28"/>
<wire x1="58.42" y1="63.5" x2="71.12" y2="63.5" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$13"/>
<wire x1="71.12" y1="63.5" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="63.5" x2="71.12" y2="58.42" width="0.1524" layer="91"/>
<junction x="71.12" y="63.5"/>
</segment>
<segment>
<wire x1="129.54" y1="27.94" x2="139.7" y2="27.94" width="0.1524" layer="91"/>
<label x="134.62" y="27.94" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$21"/>
<pinref part="GND10" gate="G$1" pin="GND"/>
<wire x1="139.7" y1="27.94" x2="139.7" y2="25.4" width="0.1524" layer="91"/>
<junction x="139.7" y="27.94"/>
<wire x1="129.54" y1="33.02" x2="139.7" y2="33.02" width="0.1524" layer="91"/>
<label x="134.62" y="33.02" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$22"/>
<wire x1="139.7" y1="33.02" x2="139.7" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$20"/>
<pinref part="GND11" gate="G$1" pin="GND"/>
<wire x1="12.7" y1="27.94" x2="7.62" y2="27.94" width="0.1524" layer="91"/>
<wire x1="7.62" y1="27.94" x2="7.62" y2="22.86" width="0.1524" layer="91"/>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$19"/>
<wire x1="12.7" y1="33.02" x2="7.62" y2="33.02" width="0.1524" layer="91"/>
<wire x1="7.62" y1="33.02" x2="7.62" y2="27.94" width="0.1524" layer="91"/>
<junction x="7.62" y="27.94"/>
</segment>
<segment>
<wire x1="129.54" y1="68.58" x2="142.24" y2="68.58" width="0.1524" layer="91"/>
<label x="134.62" y="68.58" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$29"/>
<pinref part="GND12" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$12"/>
<wire x1="12.7" y1="68.58" x2="2.54" y2="68.58" width="0.1524" layer="91"/>
<label x="5.08" y="68.58" size="1.778" layer="95"/>
<pinref part="GND13" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="1"/>
<wire x1="154.94" y1="53.34" x2="160.02" y2="53.34" width="0.1524" layer="91"/>
<wire x1="160.02" y1="53.34" x2="160.02" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND14" gate="G$1" pin="GND"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="P$2"/>
<wire x1="200.66" y1="129.54" x2="213.36" y2="129.54" width="0.1524" layer="91"/>
<wire x1="213.36" y1="129.54" x2="213.36" y2="114.3" width="0.1524" layer="91"/>
<pinref part="BOOST" gate="G$1" pin="SW"/>
<wire x1="213.36" y1="114.3" x2="208.28" y2="114.3" width="0.1524" layer="91"/>
<pinref part="SS14" gate="G$1" pin="A"/>
<junction x="213.36" y="114.3"/>
</segment>
</net>
<net name="25V" class="0">
<segment>
<pinref part="SS14" gate="G$1" pin="C"/>
<wire x1="218.44" y1="114.3" x2="223.52" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="223.52" y1="114.3" x2="231.14" y2="114.3" width="0.1524" layer="91"/>
<wire x1="231.14" y1="114.3" x2="238.76" y2="114.3" width="0.1524" layer="91"/>
<junction x="231.14" y="114.3"/>
<pinref part="R2" gate="G$1" pin="2"/>
<junction x="223.52" y="114.3"/>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="238.76" y1="114.3" x2="246.38" y2="114.3" width="0.1524" layer="91"/>
<wire x1="246.38" y1="114.3" x2="246.38" y2="111.76" width="0.1524" layer="91"/>
<junction x="238.76" y="114.3"/>
<wire x1="246.38" y1="114.3" x2="246.38" y2="137.16" width="0.1524" layer="91"/>
<junction x="246.38" y="114.3"/>
<label x="246.38" y="137.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="182.88" y1="58.42" x2="182.88" y2="68.58" width="0.1524" layer="91"/>
<label x="182.88" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="TP2" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$39"/>
<wire x1="58.42" y1="119.38" x2="63.5" y2="119.38" width="0.1524" layer="91"/>
<wire x1="63.5" y1="119.38" x2="63.5" y2="134.62" width="0.1524" layer="91"/>
<label x="63.5" y="134.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="223.52" y1="101.6" x2="223.52" y2="104.14" width="0.1524" layer="91"/>
<pinref part="BOOST" gate="G$1" pin="OVP"/>
<wire x1="223.52" y1="104.14" x2="208.28" y2="104.14" width="0.1524" layer="91"/>
<wire x1="208.28" y1="104.14" x2="208.28" y2="106.68" width="0.1524" layer="91"/>
<junction x="223.52" y="104.14"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="BOOST" gate="G$1" pin="FB"/>
<wire x1="208.28" y1="99.06" x2="208.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="208.28" y1="96.52" x2="231.14" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="231.14" y1="96.52" x2="231.14" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="231.14" y1="96.52" x2="238.76" y2="96.52" width="0.1524" layer="91"/>
<wire x1="238.76" y1="96.52" x2="238.76" y2="104.14" width="0.1524" layer="91"/>
<junction x="231.14" y="96.52"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="238.76" y1="93.98" x2="238.76" y2="96.52" width="0.1524" layer="91"/>
<junction x="238.76" y="96.52"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$34"/>
<wire x1="58.42" y1="93.98" x2="83.82" y2="93.98" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$7"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$33"/>
<wire x1="58.42" y1="88.9" x2="83.82" y2="88.9" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$32"/>
<wire x1="58.42" y1="83.82" x2="83.82" y2="83.82" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$9"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$31"/>
<wire x1="58.42" y1="78.74" x2="83.82" y2="78.74" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$10"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$30"/>
<wire x1="58.42" y1="73.66" x2="83.82" y2="73.66" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$11"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$29"/>
<wire x1="58.42" y1="68.58" x2="83.82" y2="68.58" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$12"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$26"/>
<wire x1="58.42" y1="53.34" x2="83.82" y2="53.34" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$15"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$25"/>
<wire x1="58.42" y1="48.26" x2="83.82" y2="48.26" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$16"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$24"/>
<wire x1="58.42" y1="43.18" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$17"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$23"/>
<wire x1="58.42" y1="38.1" x2="83.82" y2="38.1" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$18"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$22"/>
<wire x1="58.42" y1="33.02" x2="83.82" y2="33.02" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$19"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$21"/>
<wire x1="58.42" y1="27.94" x2="83.82" y2="27.94" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$20"/>
</segment>
</net>
<net name="P1" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$1"/>
<wire x1="12.7" y1="124.46" x2="2.54" y2="124.46" width="0.1524" layer="91"/>
<label x="5.08" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="124.46" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<label x="134.62" y="124.46" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$40"/>
</segment>
</net>
<net name="P2" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$2"/>
<wire x1="12.7" y1="119.38" x2="2.54" y2="119.38" width="0.1524" layer="91"/>
<label x="5.08" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="119.38" x2="142.24" y2="119.38" width="0.1524" layer="91"/>
<label x="134.62" y="119.38" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$39"/>
</segment>
</net>
<net name="P3" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$3"/>
<wire x1="12.7" y1="114.3" x2="2.54" y2="114.3" width="0.1524" layer="91"/>
<label x="5.08" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="114.3" x2="142.24" y2="114.3" width="0.1524" layer="91"/>
<label x="134.62" y="114.3" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$38"/>
</segment>
</net>
<net name="P4" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$4"/>
<wire x1="12.7" y1="109.22" x2="2.54" y2="109.22" width="0.1524" layer="91"/>
<label x="5.08" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="109.22" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<label x="134.62" y="109.22" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$37"/>
</segment>
</net>
<net name="P7" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$7"/>
<wire x1="12.7" y1="93.98" x2="2.54" y2="93.98" width="0.1524" layer="91"/>
<label x="5.08" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="93.98" x2="142.24" y2="93.98" width="0.1524" layer="91"/>
<label x="134.62" y="93.98" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$34"/>
</segment>
</net>
<net name="P8" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$8"/>
<wire x1="12.7" y1="88.9" x2="2.54" y2="88.9" width="0.1524" layer="91"/>
<label x="5.08" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="88.9" x2="142.24" y2="88.9" width="0.1524" layer="91"/>
<label x="134.62" y="88.9" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$33"/>
</segment>
</net>
<net name="P9" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$9"/>
<wire x1="12.7" y1="83.82" x2="2.54" y2="83.82" width="0.1524" layer="91"/>
<label x="5.08" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="83.82" x2="142.24" y2="83.82" width="0.1524" layer="91"/>
<label x="134.62" y="83.82" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$32"/>
</segment>
</net>
<net name="P10" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$10"/>
<wire x1="12.7" y1="78.74" x2="2.54" y2="78.74" width="0.1524" layer="91"/>
<label x="5.08" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="78.74" x2="142.24" y2="78.74" width="0.1524" layer="91"/>
<label x="134.62" y="78.74" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$31"/>
</segment>
</net>
<net name="P11" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$11"/>
<wire x1="12.7" y1="73.66" x2="2.54" y2="73.66" width="0.1524" layer="91"/>
<label x="5.08" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="73.66" x2="142.24" y2="73.66" width="0.1524" layer="91"/>
<label x="134.62" y="73.66" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$30"/>
</segment>
</net>
<net name="P13" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$13"/>
<wire x1="12.7" y1="63.5" x2="2.54" y2="63.5" width="0.1524" layer="91"/>
<label x="5.08" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="63.5" x2="142.24" y2="63.5" width="0.1524" layer="91"/>
<label x="134.62" y="63.5" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$28"/>
</segment>
</net>
<net name="P14" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$14"/>
<wire x1="12.7" y1="58.42" x2="2.54" y2="58.42" width="0.1524" layer="91"/>
<label x="5.08" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="58.42" x2="142.24" y2="58.42" width="0.1524" layer="91"/>
<label x="134.62" y="58.42" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$27"/>
</segment>
</net>
<net name="P15" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$15"/>
<wire x1="12.7" y1="53.34" x2="2.54" y2="53.34" width="0.1524" layer="91"/>
<label x="5.08" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="53.34" x2="142.24" y2="53.34" width="0.1524" layer="91"/>
<label x="134.62" y="53.34" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$26"/>
</segment>
</net>
<net name="P16" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$16"/>
<wire x1="12.7" y1="48.26" x2="2.54" y2="48.26" width="0.1524" layer="91"/>
<label x="5.08" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="48.26" x2="142.24" y2="48.26" width="0.1524" layer="91"/>
<label x="134.62" y="48.26" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$25"/>
</segment>
</net>
<net name="P17" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$17"/>
<wire x1="12.7" y1="43.18" x2="2.54" y2="43.18" width="0.1524" layer="91"/>
<label x="5.08" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="43.18" x2="142.24" y2="43.18" width="0.1524" layer="91"/>
<label x="134.62" y="43.18" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$24"/>
</segment>
</net>
<net name="P18" class="0">
<segment>
<pinref part="LCD-BOARD-SIDE" gate="G$1" pin="P$18"/>
<wire x1="12.7" y1="38.1" x2="2.54" y2="38.1" width="0.1524" layer="91"/>
<label x="5.08" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="129.54" y1="38.1" x2="142.24" y2="38.1" width="0.1524" layer="91"/>
<label x="134.62" y="38.1" size="1.778" layer="95"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$23"/>
</segment>
</net>
<net name="BL_ON" class="0">
<segment>
<pinref part="BOOST" gate="G$1" pin="EN"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="167.64" y1="96.52" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<wire x1="167.64" y1="99.06" x2="172.72" y2="99.06" width="0.1524" layer="91"/>
<wire x1="167.64" y1="99.06" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<junction x="167.64" y="99.06"/>
<label x="157.48" y="99.06" size="1.016" layer="95"/>
</segment>
<segment>
<wire x1="83.82" y1="119.38" x2="78.74" y2="119.38" width="0.1524" layer="91"/>
<pinref part="MAIN-BOARD-SIDE1" gate="G$1" pin="P$2"/>
<label x="83.82" y="121.92" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
